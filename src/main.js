import Vue from 'vue'
import App from './App.vue'
import VueCodemirror from 'vue-codemirror'

Vue.config.productionTip = false
Vue.use(VueCodemirror, /* {
  options: { theme: 'base16-dark', ... },
  events: ['scroll', ...]
} */)

new Vue({
  render: h => h(App),
}).$mount('#app')
